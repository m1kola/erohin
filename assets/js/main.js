(function(){
	$('.volume').slider({
		range: "min",
		min: 0,
		max: 100,
		value: 100,
		slide: function( event, ui ){
			$('.radio').prop('volume', '0.' + ui.value);
		}
	});

	var play = false;

	$('.play').click(function(){
		if(play){
			$('body').css({
				background: 'url("assets/img/tree_animation_off.gif") center'
			});
			
			$(this).text('Play').removeClass('stop');
			$('.radio')[0].pause();
			play = false;
		}else{
			$('body').css({
				background: 'url("assets/img/tree_animation_on.gif") center'
			});
			$(this).text('Stop').addClass('stop');
			$('.radio')[0].play();
			play = true;
		}
	});

	$('.play').hover(function(){
		
	});

	setInterval(function(){
		$('.time').html(simpleJqueryClock());
	}, 1000);
})();


function simpleJqueryClock(){
	var date = new Date();

	currentHours = date.getHours();
	currentHours = ("0" + currentHours).slice(-2);

	currentMinutes = date.getMinutes();
	currentMinutes = ("0" + currentMinutes).slice(-2);

	return currentHours + '<span class="blink">:</span>' + currentMinutes;
}